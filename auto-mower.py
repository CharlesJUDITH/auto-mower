#!/usr/bin/python3
"""
Auto-mower program
"""

from __future__ import absolute_import
import argparse
import logging
import socket

class Mower:
    ''' Initialize Mower class '''
    def __init__(self, x, y, o, limit_x, limit_y):
        '''Define mower object'''
        self.x = x
        self.y = y
        self.o = o
        self.limit_x = limit_x
        self.limit_y = limit_y

    def mower_position(self):
        ''' Define Mower position x,y,o'''
        return str(self.x), str(self.y), self.o

    def forward(self):
        ''' Forward function '''
        if self.o == "S" and self.y > 0:
            # We're using Subtraction Assignment here (+=, -=)
            self.y -= 1
        elif self.o == "N" and self.y < self.limit_y:
            self.y += 1
        elif self.o == "E" and self.x > 0:
            self.x -= 1
        elif self.o == "W" and self.x < self.limit_x:
            self.x += 1

    def turn(self, direction="R"):
        ''' Turn function '''
        orientation = ['N', 'E', 'S', 'W']
        current_position = orientation.index(self.o)
        if direction == "L":
            log.info('Turning left. Direction: %s' % direction)
            try:
                self.o = orientation[current_position + 1]
            except IndexError:
                self.o = "N"
        else:
            log.info('Turning right. Direction: %s' % direction)
            try:
                self.o = orientation[current_position - 1]
            except IndexError:
                self.o = "W"
        return self.mower_position()

    def order(self, instruction):
        ''' Instructions orders '''
        if instruction == "F":
            log.info('Moving forward. Instruction: %s' % instruction)
            self.forward()
        else:
            self.turn(instruction)
        return self.mower_position()

class Guarden:
    ''' Initialize Guarden class '''
    def __init__(self, line):
        # Split x and y with the "space"
        self.limit_x, self.limit_y = line.split(" ")

    def setup_mower(self, line):
        ''' Setup mower '''
        x, y, o = line.split(" ")
        self.m = Mower(int(x), int(y), o, int(self.limit_x), int(self.limit_y))
        self.run = [(x, y, o)]
        return self.m

    def get_mower_position(self):
        ''' Function to get the mower position '''
        return " ".join(self.m.mower_position())

    def start(self, instruction):
        ''' Start mower with insctructions'''
        for i in instruction:
            self.m.order(i)
            self.run.append(self.get_mower_position())
        return self

def run_mower(data):
    ''' Run mower '''
    positions = []
    for i, line in enumerate(data):
        line = line.strip()
        if line != "":
            if i == 0:
                g = Guarden(line)
            elif i % 2 != 0:
                if line in positions:
                    log.info('There is already a mower here.')
                else:
                    g.setup_mower(line)
            else:
                log.info('Current line: %s' % line)
                g.start(line)
                log.info('Mower ran at %s' % g.run)
                positions.append(g.get_mower_position())
    for p in positions:
        print(p)
        log.info('positions: %s' % p)

def read_file(file):
    ''' Open data instructions file '''
    with open(file, 'r') as f:
        data = f.read().split("\n")
    return data

def cut_lawn(file):
    data = read_file(file)
    return run_mower(data)

def create_logger(log_level=logging.INFO, stdout=True, logfile=None):
    """Create a logging object, able to log in a file and/or stdout"""

    # Create logger and set logging level threshold
    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)

    # Set log format for the handlers
    formatter = logging.Formatter('[%(asctime)s] %(host)s %(app_name)s \
        %(levelname)s [%(process)d]: %(message)s')

    if stdout:
        # Create a stream logger handler
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(log_level)
        stream_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)

    if logfile:
        # Create a file logger handler
        file_handler = logging.FileHandler(logfile)
        file_handler.setLevel(log_level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    # Set parameters value for the logging format
    hostname = socket.gethostname()
    extlogger = logging.LoggerAdapter(logger, {'host': hostname, 'app_name': 'auto-mower'})

    return extlogger

def main():
    '''Main function'''
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--file", help="Instructions file", required=True)
    parser.add_argument("-l", "--logfile", help="Where to log", default=None)
    args = parser.parse_args()

    global file
    file = args.file
    global logfile
    logfile = args.logfile
    global log
    log = create_logger(log_level=logging.INFO, stdout=False, logfile=logfile)

    cut_lawn(file)

if __name__ == "__main__":

    main()
